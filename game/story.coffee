import dialogue from './_dialogue'
import phrase from './_phrase'

salet.game_id = "your-game-id-here"
salet.game_version = "1.6"
$(document).ready(() ->
  window.addEventListener('popstate', (event) ->
    salet.goBack()
  )
  $("#night").on("click", () ->
    if (window.night)
      styles = {
        "-webkit-filter": ""
        "filter": ""
        "background-color": ""
      }
      $("body").css(styles)
      $("#night").removeClass("active")
      window.night = false
    else
      styles = {
        "-webkit-filter": "invert(1)hue-rotate(180deg)"
        "filter": "invert(1)hue-rotate(180deg)"
        "background-color": "#000"
      }
      $("body").css(styles)
      $("#night").addClass("active")
      window.night = true
  )

  salet.beginGame()
)

###
Element helpers. There is no real need to build monsters like a().id("hello")
because you won't use them as is. It does not make sense in context, the
author has Markdown and all utilities to *forget* about the markup.
###
way_to = (content, ref) ->
  return "<a href='#{ref}' class='way'>#{content}</a>"
textlink = (content, ref) ->
  return "<a href='./_writer_#{ref}' class='once'>#{content}</a>"
actlink = (content, ref) ->
  return "<a href='./#{ref}' class='once'>#{content}</a>"

# The first room of the game.
# For accessibility reasons the text is provided in HTML, not here.
room "start",
  enter: () ->
    salet.character.bought_lamp = false
  dsc: """
  """,
  choices: "#start"

# This is a special inventory room.
# The inventory button is a regular link to this room.
# You may alter these as much as you like or scrap it along with the button.
room "inventory",
  canSave: false # saving the game here is forbidden. Aautosaving too.
  enter: () ->
    $("#inventory").hide()
  exit: () ->
    $("#inventory").show()
  dsc: () ->
    if salet.character.inventory.length == 0
      text = "You are carrying nothing."
    else
      text = "You are carrying:\n\n"
      for thing in salet.character.inventory
        text += "* #{salet.character.listinv(thing.name)}\n"
    return text+"\n\n"+"""
    <div class="center"><a href="./exit"><button class="btn btn-lg btn-outline-primary">Go back</button></a></div>
    """
  actions:
    exit: () ->
      return salet.goBack()

room "world",
  tags: ["start"],
  optionText: "Enter the world",
  ways: ["plaza"]
  dsc: """
    ### Rhinestone Room

    You're in a large room carved inside a giant milky rock mountain.
    The floor and walls are littered with signs and signatures of the previous visitors.
  """
  units: [
    unit "well",
      dsc: "A steep narrow {{well}} proceeds upward."
      act: "There is only one passage out. See the „Other rooms“ block popped up? Click it."
  ]

room "plaza",
  title: (from) ->
    if from == "world"
      return "Upwards"
    else
      return "Town plaza"
  cycle: ["quirky", "distinct", "kooky", "crazy", "quaint"]
  ways: ["shop"]
  before: (from) ->
    if from == 'world'
      """
        You climb up the well and come out to a central plaza of a #{salet.view.cycleLink("quaint")} little town.
        A plaque nearby says it's the town of *Innsmouth,* wherever that is.
      """
    else
      "You quickly find the central plaza."
  units: [
    unit "policeman",
      dsc: "There is a policeman nearby. You could ask him {{for directions.}}"
      act: () ->
        if salet.character.has_mark?
          return "You already talked to him, no need to bug the man twice."
        salet.character.has_mark ?= true
        salet.getRoom("lair")
        """
          “Here, let me mark it on your map.”
        """
    unit "people",
      dsc: "There are {{people shouting}} nearby."
      act: 'Just some weirdos shouting "Viva la Cthulhu!". Typical.'
  ]
  writers:
    cyclewriter: () ->
      responses = @cycle
      if typeof responses == "function"
        responses = responses()
      cycleIndex = window.localStorage.getItem("cycleIndex")
      cycleIndex ?= 0
      response = responses[cycleIndex]
      cycleIndex++
      if cycleIndex == responses.length
        cycleIndex = 0
      window.localStorage.setItem("cycleIndex", cycleIndex)
      return salet.view.cycleLink(response)

room "shop",
  title: "The Shop"
  #pic: "http://loremflickr.com/640/300/room,shop"
  ways: ["plaza", "shop-inside", "lair"]
  dsc: """
    Being the only shop in town, this trendy establishment did not need a name.
    It's an open question why it had one, especially because its name was "Hung Crossing".

    You are standing in front of a picturesque sign. It's cold here.
  """

room "lair",
  title: "The Lair"
  before: "Finding The Lair is easy. Leaving it is impossible. Your game ends here."
  dsc: """
    The Lair of Yog-Sothoth is a very *n'gai* cave, full of *buggs-shoggogs* and *n'ghaa ng'aa*.
  """
  ways: ["shop"]
  units: [
    unit "bugg",
      dsc: "You see a particularly beautiful slimy {{bugg.}}"
      takeable: false
      display: "bugg"
      act: () =>
        salet.rooms[salet.current].drop("bugg")
        return "You eat the bugg mass. Delicious and raw. Perhaps it's a good lair to live in."
  ]

phrase "Yes", "merchant", """
  Yes.
"""
dialogue "No", "merchant", "merchant", """
  No.
"""
room "sell-lamp",
  ways: ["shop"]
  tags: ["merchant"]
  choices: ["#merchant"]
  optionText: "May I buy this lamp?"
  title: "Talking with merchant"
  canView: () ->
    return salet.character.has("lamp") and salet.character.bought_lamp == false
  enter: () ->
    salet.character.bought_lamp = true
  dsc: """
    "That'll be 30 pieces of your time."

    You quickly pay the price and take the lamp as a rightful owner.
  """

room "shop-inside",
  ways: ["shop"]
  tags: ["merchant"]
  optionText: "End the conversation"
  title: "Inside the Shop"
  dsc: """
    The insides are painted pastel white, honouring The Great Milk Spill of 1985.
  """
  units: [
    unit "merchant",
      dsc: "A {{merchant}} eyes you warily."
      takeable: false
      act: () =>
        salet.processClick("merchdialogue")
        return ""
  ]

lamp = unit "lamp",
  display: "lamp"
  takeable: true
lamp.put("shop-inside")

# The dialogue entry point has to be a room, in order to have an ID to go to.
room "merchdialogue",
  choices: "#merchant",
  dsc: """
    Nice day, isn't it?
  """
