const mix = require('laravel-mix');

// Public path helper
const publicPath = (path) => `dist/${path}`;

// Source path helper
const src = (path) => `./${path}`;

// Public Path
mix
  .setPublicPath('./dist')
  .setResourceRoot('/');

// Browsersync
mix.browserSync('example.test');

// CoffeeScript
mix.webpackConfig({
  module: {
    rules:[
      {
        test: /\.coffee$/,
        loader: 'coffee-loader'
      }
    ]
  }
});

// Styles
mix.sass(src`sass/main.scss`, 'styles')
  .options({
    processCssUrls: false,
  })
  .extract();

// JavaScript
mix.js('game/story.coffee', 'game')
  .extract(); // extract vendor libraries

// Assets
// mix.copyDirectory(src`images`, publicPath`images`);
// mix.copyDirectory(src`fonts`, publicPath`fonts`);
mix.copyDirectory(src`html`, publicPath``);

// Autoload
/*
mix.autoload({
  jquery: ['$', 'window.jQuery'],
});
*/

// Options
mix.options({
  processCssUrls: false,
});

// Source maps when not in production.
mix.sourceMaps(false, 'source-map');

// Hash and version files in production.
// mix.version();
